# -*- text -*-
#
#  $Id: fa04cdabb71767050aaa0664da792fd6086adb19 $

#  Persistent, embedded Perl interpreter.
#
perl perl_acct {
	#
	#  The Perl script to execute on authorize, authenticate,
	#  accounting, xlat, etc.  This is very similar to using
	#  'rlm_exec' module, but it is persistent, and therefore
	#  faster.
	#
	filename = ${modconfdir}/${.:instance}/toKafka_renameNETKafka.pl

	#
	#  Options which are passed to the Perl interpreter.
	#  These are (mostly) the same options as are passed
	#  to the "perl" command line.
	#
	#  The most useful flag is "-T".  This sets tainting on.  And
	#  as of 3.0.18, makes it impossible to leverage bad
	#  User-Names into local command execution.
	#
	perl_flags = "-T"

	config {
                # Kafka 
                Kafka {
                        # bootstrap server; no default
                        # must be set or freeradius will exit on startup
                        bootstrap-servers = "kafka1, kafka2"
                        #bootstrap-servers = "kafka1, kafk2"

                        # topic messages will be send to; no default
                        # must be set or freeradius will exit on startup
                        topic = "radius_json"
                }
                # Rename Freeradius names to database columns
                Rename {
                        'Event-Timestamp' = "eventtimestampstart"
                        'Acct-Status-Type' = 'acctstatustype'
                        'Acct-Unique-Session-Id' = 'acctuniquesessionid'
                        'Acct-Input-Octets' = 'acctinputoctets'
                        'Acct-Output-Octets' = 'acctoutputoctets'
                        'Acct-Input-Packets' = 'acctinputpackets'
                        'Acct-Output-Packets' = 'acctoutputpackets'
                        'Calling-Station-Id' = 'callingstationid'
                        'Called-Station-Id' = 'calledstationid'
                        '3GPP-IMSI' = '_3gppimsi'
                        '3GPP-IMEISV' = '_3gppimeisv'
                        'NAS-IP-Address' = 'nasipaddress'
                        'Framed-IPv6-Prefix' = 'framedipv6prefix'
                        'Delegated-IPv6-Prefix' = 'delegatedipv6prefix'
                        '3GPP-IMSI-MCC-MNC' = '_3gppimsimccmnc'
                        '3GPP-SGSN-MCC-MNC' = '_3gppsgsnmccmnc'
                        '3GPP-RAT-Type' = '_3gpprattype'
                        '3GPP-Location-Info' = '_3gpplocationinfo'
                        'Acct-Terminate-Cause' = 'acctterminatecause'
                }
        }

	#
	#  The following hashes are given to the module and
	#  filled with value-pairs (Attribute names and values)
	#
	#  %RAD_CHECK               Check items
	#  %RAD_REQUEST             Attributes from the request
	#  %RAD_REPLY               Attributes for the reply
	#  %RAD_REQUEST_PROXY       Attributes from the proxied request
	#  %RAD_REQUEST_PROXY_REPLY Attributes from the proxy reply
	#
	#  The interface between FreeRADIUS and Perl is strings.
	#  That is, attributes of type "octets" are converted to
	#  printable strings, such as "0xabcdef".  If you want to
	#  access the binary values of the attributes, you should
	#  call the Perl "pack" function.  Then to send any binary
	#  data back to FreeRADIUS, call the Perl "unpack" function,
	#  so that the contents of the hashes are printable strings.
	#
	#  IP addresses are sent as strings, e.g. "192.0.2.25", and
	#  not as a 4-byte binary value.  The same applies to other
	#  attribute data types.
	#
	#  Attributes of type "string" are copied to Perl as-is.
	#  They are not escaped or interpreted.
	#
	#  The return codes from functions in the perl_script
	#  are passed directly back to the server.  These
	#  codes are defined in mods-config/example.pl
	#

	# You can define configuration items (and nested sub-sections) in perl "config" section.
	# These items will be accessible in the perl script through %RAD_PERLCONF hash.
	# For instance: $RAD_PERLCONF{'name'} $RAD_PERLCONF{'sub-config'}->{'name'}
	#
	#config {
	#	name = "value"
	#	sub-config {
	#		name = "value of name from config.sub-config"
	#	}
	#}

	#
	#  List of functions in the module to call.
	#  Uncomment and change if you want to use function
	#  names other than the defaults.
	#
	#func_authenticate = authenticate
	#func_authorize = authorize
	#func_preacct = preacct
	#func_accounting = accounting
	#func_checksimul = checksimul
	#func_pre_proxy = pre_proxy
	#func_post_proxy = post_proxy
	#func_post_auth = post_auth
	#func_recv_coa = recv_coa
	#func_send_coa = send_coa
	#func_xlat = xlat
	#func_detach = detach

	#
	#  Uncomment the following lines if you wish
	#  to use separate functions for Start and Stop
	#  accounting packets. In that case, the
	#  func_accounting function is not called.
	#
	#func_start_accounting = accounting_start
	#func_stop_accounting = accounting_stop
}

perl perl_post_auth {
	filename = ${modconfdir}/${.:instance}/toKafka_postAuth.pl
	perl_flags = "-T"
	config {
                # Kafka 
                Kafka {
                        # bootstrap server; no default
                        # must be set or freeradius will exit on startup
                        bootstrap-servers = "kafka1, kafka2"
                        #bootstrap-servers = "kafka1, kafk2"

                        # topic messages will be send to; no default
                        # must be set or freeradius will exit on startup
                        topic = "radius_post_auth"
                }
                # Rename Freeradius names to database columns
                Rename {
                        'User-Name' = "username"
                        'User-Password' = 'pass'
                        'Module-Failure-Message' = 'error'
                        'Called-Station-Id' = 'calledstationid'
                        '3GPP-IMSI' = 'imsi'
                }
        }
	func_post_auth = post_auth
}
