
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#  Copyright 2002  The FreeRADIUS server project
#  Copyright 2002  Boian Jordanov <bjordanov@orbitel.bg>
#

#
# Example code for use with rlm_perl
#
# You can use every module that comes with your perl distribution!
#
# If you are using DBI and do some queries to DB, please be sure to
# use the CLONE function to initialize the DBI connection to DB.
#

use strict;
use warnings;
use threads;

# use ...
use Data::Dumper;

use Net::Kafka::Producer;
use Net::Kafka qw/
  RD_KAFKA_PRODUCER
  RD_KAFKA_PARTITION_UA
  RD_KAFKA_EVENT_DR
  RD_KAFKA_EVENT_NONE
  RD_KAFKA_EVENT_STATS
  RD_KAFKA_EVENT_ERROR
  /;
  use POSIX qw/EAGAIN/;

# Bring the global hashes into the package scope
our ( %RAD_REQUEST, %RAD_REPLY, %RAD_CHECK, %RAD_STATE, %RAD_PERLCONF );

# This is hash wich hold original request from radius
#my %RAD_REQUEST;
# In this hash you add values that will be returned to NAS.
#my %RAD_REPLY;
#This is for check items
#my %RAD_CHECK;
# This is the session-sate
#my %RAD_STATE;
# This is configuration items from "config" perl module configuration section
#my %RAD_PERLCONF;

# Multi-value attributes are mapped to perl arrayrefs.
#
#  update request {
#    Filter-Id := 'foo'
#    Filter-Id += 'bar'
#  }
#
# This results to the following entry in %RAD_REQUEST:
#
#  $RAD_REQUEST{'Filter-Id'} = [ 'foo', 'bar' ];
#
# Likewise, you can assign an arrayref to return multi-value attributes

#
# This the remapping of return values
#
use constant {
	RLM_MODULE_REJECT   => 0,    # immediately reject the request
	RLM_MODULE_OK       => 2,    # the module is OK, continue
	RLM_MODULE_HANDLED  => 3,    # the module handled the request, so stop
	RLM_MODULE_INVALID  => 4,    # the module considers the request invalid
	RLM_MODULE_USERLOCK => 5,    # reject the request (user is locked out)
	RLM_MODULE_NOTFOUND => 6,    # user not found
	RLM_MODULE_NOOP     => 7,    # module succeeded without doing anything
	RLM_MODULE_UPDATED  => 8,    # OK (pairs modified)
	RLM_MODULE_NUMCODES => 9     # How many return codes there are
};

# Same as src/include/log.h
use constant {
	L_AUTH         => 2,     # Authentication message
	L_INFO         => 3,     # Informational message
	L_ERR          => 4,     # Error message
	L_WARN         => 5,     # Warning
	L_PROXY        => 6,     # Proxy messages
	L_ACCT         => 7,     # Accounting messages
	L_DBG          => 16,    # Only displayed when debugging is enabled
	L_DBG_WARN     => 17,    # Warning only displayed when debugging is enabled
	L_DBG_ERR      => 18,    # Error only displayed when debugging is enabled
	L_DBG_WARN_REQ =>
	  19,    # Less severe warning only displayed when debugging is enabled
	L_DBG_ERR_REQ =>
	  20,    # Less severe error only displayed when debugging is enabled
};

#  Global variables can persist across different calls to the module.
#
#
#	{
#	 my %static_global_hash = ();
#
#		sub post_auth {
#		...
#		}
#		...
#	}
# Kafka connection

#our ( $condvar, $producer, $promise );
our ( $producer, $promise );

# Buffer
our $count  = 0;
our $alive  = 0;
our $random = 0;
our $kafka_servers;
our $kafka_topic;

# you need this or freeradius will eventually die
$SIG{'ALRM'} = \&detach;

#$SIG{'TERM'} = \&detach;
#$SIG{'INT'} = \&detach;

# get the kafka server from config; die if there is none
if ( !defined $RAD_PERLCONF{'Kafka'}->{'bootstrap-servers'} ) {
	&radiusd::radlog( L_DBG,
"PERL ERROR: please specify bootstrap-servers in perl config section; EXIT"
	);
	kill 'TERM', 0;
}
else {
	$kafka_servers = $RAD_PERLCONF{'Kafka'}->{'bootstrap-servers'};
}

# get the kafka topic from config; die if there is none
if ( !defined $RAD_PERLCONF{'Kafka'}->{'topic'} ) {
	&radiusd::radlog( L_DBG,
		"PERL ERROR: please specify kafka topic in perl config section; EXIT" );
	kill 'TERM', 0;
}
else {
	$kafka_topic = $RAD_PERLCONF{'Kafka'}->{'topic'};
}

#&log_config_attributes;

# Don't do it here, or you will crash the server
# wait till it is forked away
#$producer = Net::Kafka::Producer->new(
#	'bootstrap.servers' => "$kafka_server:$kafka_port",
#);

# NOTE
# because embeded perl does not allow callback methods we do have to remodel
# eventhandling here ourselfs
sub _process_event_delivery_reports {

	#&radiusd::radlog(L_ERR,"_process_event_delivery_reports");
	my ( $self, $event ) = @_;
	while ( my $report = $event->event_delivery_report_next() ) {
		my $msg_id = $report->{msg_id};
		if ( exists $report->{err} ) {
			&radiusd::radlog( L_ERR,
				"Error AUTH on delivery: " . $report->{err} );

			$self->_reject_deferred(
				$msg_id,
				{
					error      => $report->{err_msg},
					error_code => $report->{err},
				}
			);
		}
		else {
			# Good, no further action
			#&radiusd::radlog(L_ERR,"Good on delivery: ". $report->{offset});
			$self->_resolve_deferred(
				$msg_id,
				{
					offset    => $report->{offset},
					partition => $report->{partition},
					timestamp => $report->{timestamp},
				}
			);
		}
	}
}

sub _process_event_error {
	my ( $self, $event ) = @_;
	my $err     = $event->event_error();
	my $err_msg = $event->event_error_string();
	&radiusd::radlog( L_ERR, "MSG AUTH: $err $err_msg" );

}

sub _process_events {

	#&radiusd::radlog(L_ERR,"_process_events");
	#my $self = shift;
	my $self = $producer;
	while ( my $event = $self->{_kafka}->queue_poll() ) {
		if ( !defined $event ) {
			&radiusd::radlog( L_ERR, "ERROR AUTH: empty event received" );
			next;
		}

		#print "GOT EVENT\n";
		my $event_type = $event->event_type();
		if ( $event_type == RD_KAFKA_EVENT_NONE ) {

			#print "Do nothing";

			# do nothing
		}
		elsif ( $event_type == RD_KAFKA_EVENT_DR ) {
			#print "RD_KAFKA_EVENT_DR\n";
			_process_event_delivery_reports( $self, $event );
		}
		elsif ( $event_type == RD_KAFKA_EVENT_STATS ) {
			#print "RD_KAFKA_EVENT_STATS\n";

			# $self->_process_event_stats($event);
		}
		elsif ( $event_type == RD_KAFKA_EVENT_ERROR ) {
			#print "RD_KAFKA_EVENT_ERROR\n";
			&radiusd::radlog( L_ERR,
				"AUTH RD_KAFKA_EVENT_ERROR random=$random" );
			_process_event_error( $self, $event );
		}
		else {
			&radiusd::radlog( L_WARN,
				"AUTH unknown event type %s received" . $event_type );
		}
	}
}

sub _signal_watcher_cb {
    my $self = $producer;
    return if $self->{_is_closed};

    my $signals_count = sysread $self->{_read_queue_fd}, my $signal, 1;
    if (! defined $signals_count) {
        $! == EAGAIN and return;
        warn "sysread failed: $!";
    } else {
        _process_events();
    }
}

#
# end of event handling

# Function to handle post authentication
sub post_auth {
	eval {
		if ( !$producer ) {
			$alive++;
			$random = int( rand(30000) );
			&radiusd::radlog( L_INFO,
				"CREATE AUTH random=$random alive=$alive" );
			$producer = Net::Kafka::Producer->new(
				'bootstrap.servers' => "$kafka_servers", );
		}

		# For debugging purposes only
		#&log_request_attributes;
		#&log_config_attributes;

		my $message = make_post_auth_json_message();

		#print "PERL: sending message\n";
		# just fire and forget
		$promise = $producer->produce(
			payload => $message,
			topic   => $kafka_topic

			  #);
		);

		#print "PROD:" . Dumper( $producer->{_in_flight} );

# Because events are handled by callbacks and that does not work in embeded perl
# we have to handle incoming messages ourselfe
		#_process_events();
		_signal_watcher_cb ;
		 if ( ($producer->{_max_id} % 5000) == 0){
                        $count = $producer->{_max_id};
                        my $in_flight = keys %{$producer->{_in_flight}};
                        &radiusd::radlog( L_INFO, "PERL AUTH random=$random alive=$alive count=$count in_flight=$in_flight" );
                }

		#print "PROD:" . Dumper( $producer->{_in_flight} );
	};
	if ($@) {
		&radiusd::radlog( L_ERR, "PERL AUTH random=$random alive=$alive $@" );
	}
	return RLM_MODULE_OK;

}

# Function to handle checksimul
sub checksimul {

	# For debugging purposes only
	#	&log_request_attributes;

	return RLM_MODULE_OK;
}

# Function to handle xlat
sub xlat {

	# For debugging purposes only
	#	&log_request_attributes;

	# Loads some external perl and evaluate it
	my ( $filename, $a, $b, $c, $d ) = @_;
	&radiusd::radlog( L_DBG, "From xlat $filename " );
	&radiusd::radlog( L_DBG, "From xlat $a $b $c $d " );
	local *FH;
	open FH, $filename or die "open '$filename' $!";
	local ($/) = undef;
	my $sub = <FH>;
	close FH;
	my $eval = qq{ sub handler{ $sub;} };
	eval $eval;
	eval { main->handler; };
}

# Function to handle detach
sub detach {

	# For debugging purposes only
	#	&log_request_attributes;
	my $in_flight=0;
	if ( $producer){
		$in_flight = keys %{$producer->{_in_flight}};
		my $count = $producer->{_max_id};
		$producer->close() if defined $producer;
	}
	&radiusd::radlog( L_INFO,
		"DETACH AUTH random=$random alive=$alive count=$count in_flight=$in_flight" );
	$alive--;
}

# generate a message string in JSON format
# build it from all Request parameters
sub make_post_auth_json_message {
	my $m = "{";
	if ( defined $RAD_PERLCONF{'Rename'} ) {
		for ( keys %RAD_REQUEST ) {
			$RAD_REQUEST{$_} =~ s/\"/\'/g;
			if ( defined $RAD_PERLCONF{'Rename'}->{$_} ) {
				$m .=
				  "\"$RAD_PERLCONF{'Rename'}->{$_}\":\"$RAD_REQUEST{$_}\", ";
			}
			else {
				$m .= "\"$_\":\"$RAD_REQUEST{$_}\", ";
			}
		}
	}
	else {
		for ( keys %RAD_REQUEST ) {
			$RAD_REQUEST{$_} =~ s/\"/\'/g;
			$m .= "\"$_\":\"$RAD_REQUEST{$_}\", ";
		}
	}
	if ( defined $RAD_CHECK{'Post-Auth-Type'}
		&& $RAD_CHECK{'Post-Auth-Type'} =~ m/Reject/ )
	{
 #&radiusd::radlog(L_DBG,"PERL Post-Auth-Type: ". $RAD_CHECK{'Post-Auth-Type'});
		$m .= "\"reply\": \"Access-Reject\"";
	}
	else {
		#&radiusd::radlog(L_DBG,"PERL Auth-Type: Access-Accept");
		$m .= "\"reply\": \"Access-Accept\"";
	}

	# delete last ", " or confluent ksql can't parse it
	$m =~ s/, $//;
	$m .= "}";
	return $m;
}

sub END {

	#&radiusd::radlog(0, "PERL: Destroy");
	&radiusd::radlog( L_INFO, "END AUTH random=$random" );
	detach();
}

#
# Some functions that can be called from other functions
#
sub log_request_attributes {

	# This shouldn't be done in production environments!
	# This is only meant for debugging!
	for ( keys %RAD_REQUEST ) {
		&radiusd::radlog( L_DBG, "RAD_REQUEST: $_ = $RAD_REQUEST{$_}" );
	}
}

sub log_config_attributes {

	# This shouldn't be done in production environments!
	# This is only meant for debugging!

	#for (keys %RAD_PERLCONF) {

	#	&radiusd::radlog(L_DBG, "RAD_PERLCONF $_ = $RAD_PERLCONF{$_}");

	#}
	my $d    = Data::Dumper->new( [ \%RAD_PERLCONF ] );
	my $data = $d->Dump( [%RAD_PERLCONF] );

	#print $d->Dump;
	&radiusd::radlog( L_DBG, "PERL: Data Dumper" );
	my $s = $RAD_PERLCONF{'test'};
	&radiusd::radlog( L_DBG, "PERL: $data" );

	#&radiusd::radlog(L_DBG, Dumper(\%RAD_PERLCONF));
}
