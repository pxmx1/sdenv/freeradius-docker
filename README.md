# Build a standalone single node Zookeeper/Kafka/Cassandra/Flink Cluster on Proxmox VE


## Usage

`inventory/my-cluster/hosts.ini` und `inventory/my-cluster/group_vars/all/*` anpassen:

### VM Installation
Installieren der VM:
```
    ansible-playbook site-vm.yml
```
Benötigt FreeIPA und Proxmox

### Cassandra, Druid, Flink, Kafka, Zookeeper, Kafka-UI
Installieren der Services:
```
    ansible-playbook site-kfc.yml
```

### NIFI
Installieren von nifi:
```
    ansible-playbook site-nifi.yml
```
Anmelden mit admin/password1234


## Nach der Installation:
 - Kafka UI: `http://test.my.lan:9090`
 - Flink UI: `http://test.my.lan:8081`
 - Nifi UI: `https://test.my.lan:8443/nifi`
 - Druid:`https://test.my.lan:8888`

## Anmerkungen

Portänderungen abweichend vom Default:
 - Druid coordinator-overload von 8081 zu 7081

Kakfa
 - 9092

Kafka UI:
 - 9090

Flink
 - 8081
 - 6123

Cassandra:
 - 7000
 - 7199
 - 9042

Zookeeper:
 - 8080
 - 2181

Nifi:
 - 8443  

Druid:
 - 8082
 - 8083
 - 1527
 - 8888
 - 8091
 - 7081 (8081->7081)

## Weiterführendes
zu Trino:
 - https://github.com/bitsondatadev/trino-getting-started
 - https://github.com/sorieux/trino-kafka-demo
 - https://docs.starburst.io/clients/dbeaver.html
 - https://github.com/sorieux/trino-config-example/tree/main
 - https://github.com/sorieux/trino-catalog-example