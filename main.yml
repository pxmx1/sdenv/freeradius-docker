---
# - name: Check If Docker Is Installed
#   hosts: buildhost
#   tasks:
#     - name: get docker version
#       ansible.builtin.shell: 
#         cmd: docker --version
#       register: docker_valid
#       ignore_errors: yes

# - name: Add Role docker if not installed
#   hosts: buildhost
#   gather_facts: yes
#   become: yes
#   roles:
#     - role: docker_ce
#       when: docker_valid.failed

- name: setup freeradius build 
  hosts: buildhost
  gather_facts: no
  vars:
    freeradhome: '/home/ansible/repos/freeradius'
    dockername: "freeradius-extended"
    dockertag: "V3.2.3"
    dockerregistry: "192.168.193.10:5000"
    rlm_kafka_url: 'https://github.com/thebinary/rlm_kafka.git'
    url_cassandra_cpp_driver:     'https://datastax.jfrog.io/artifactory/cpp-php-drivers/cpp-driver/builds/2.17.1/e05897d/ubuntu/22.04/cassandra/v2.17.1/cassandra-cpp-driver_2.17.1-1_amd64.deb'
    url_cassandra_cpp_driver_dev: 'https://datastax.jfrog.io/artifactory/cpp-php-drivers/cpp-driver/builds/2.17.1/e05897d/ubuntu/22.04/cassandra/v2.17.1/cassandra-cpp-driver-dev_2.17.1-1_amd64.deb'
    url_rlm_sql_cassandra: 'https://gist.github.com/linnaea/ef2457ae6fc6db7c2e04/archive/2be18fc708f146eaff8806158509f3b02cedaee6.zip'
    dockerbuilddir: "{{freeradhome}}/scripts/docker/ubuntu22_extended"

  tasks:
    - name: apt install dependencies
      block:
        - name: build dependencies
          ansible.builtin.apt:
            name: [build-essential, devscripts, quilt, debhelper, fakeroot, equivs , 
              libmongoc-dev, libbson-dev, librdkafka-dev, libtalloc-dev, libkqueue-dev, 
              asciidoctor, pandoc, doxygen, 
              snmp, libpcap-dev, libcollectdclient-dev, libcap-dev, libcrypto++-dev, libssl-dev, graphviz 
            ] #  antora 
            state: present      

        - name: deb pkg build dependencies
          ansible.builtin.apt:
            name: [ libiodbc2-dev, libsystemd-dev , 
              freetds-dev , libcurl4-openssl-dev , libgdbm-dev , libiodbc2-dev , libjson-c-dev , 
              libkrb5-dev , libldap2-dev , libpam0g-dev , libperl-dev , default-libmysqlclient-dev , 
              libmysqlclient-dev , libpq-dev , libreadline-dev , libsasl2-dev , libsqlite3-dev , libunbound-dev , 
              libwbclient-dev , libyubikey-dev , libykclient-dev , libmemcached-dev , libhiredis-dev , 
              python3-dev , python2-dev , samba-dev , libidn11-dev , libpcre2-dev , libpcre3-dev , unixodbc-dev
            ] #  libcurl4-gnutls-dev, libjson-c3, heimdal-dev, python-dev, samba4-dev   
            state: present      
          tags: debdep
      become: true
      tags: [builddebs, build]

    - name: build freeradius locally
      block:
        - name: clone Freeradius
          git:  
            repo: https://github.com/FreeRADIUS/freeradius-server.git
            #version: release_3_2_3
            version: origin/v3.2.x
    #        version: release_4
            dest: '{{freeradhome}}'
            clone: yes
            update: yes

        - name: freeradius deb building preparation
          block:
            - name: install cassandra driver + dev driver
              become: true
              ansible.builtin.apt:
                deb: '{{url_cassandra_cpp_driver}}'
            - name: install cassandra driver + dev driver
              become: true
              ansible.builtin.apt:
                deb: '{{url_cassandra_cpp_driver_dev}}' 

            # https://gist.github.com/linnaea/ef2457ae6fc6db7c2e04#file-rlm_sql_cassandra-c
            - name: get rlm_sql_cassandra
              ansible.builtin.get_url:
                url: '{{url_rlm_sql_cassandra}}'
                dest: '{{freeradhome}}/src/modules/rlm_sql/drivers/rlm_sql_cassandra.zip'
                mode: '0660'
            - name: Create directory and unarchive
              ansible.builtin.file:
                path: '{{freeradhome}}/src/modules/rlm_sql/drivers/rlm_sql_cassandra'
                state: directory
                mode: 0755   # optional: set the directory permissions
            - name: Unarchive cassandra
              ansible.builtin.unarchive:
                src:  '{{freeradhome}}/src/modules/rlm_sql/drivers/rlm_sql_cassandra.zip'
                dest: '{{freeradhome}}/src/modules/rlm_sql/drivers/rlm_sql_cassandra'
                remote_src: yes
                extra_opts: 
                  - -o
          tags: [builddebs, build]

        - name: build freeradius
          block:
            - name: clone rlm_kafka
              ansible.builtin.shell: |  
                rm -rf rlm_kafka && git clone '{{rlm_kafka_url}}'
              args:
                chdir: '{{freeradhome}}/src/modules'

            - name: configure freeradius with experimental modules
              ansible.builtin.shell: |  
                export confflags=--with-experimental-modules && ./configure 
#               make && make install && make deb 
              args:
                chdir: '{{freeradhome}}'
          tags: [build, make]


    # - name: build docker
    #   block:    
    #     - name: create build directory
    #       file:
    #         path: '{{dockerbuilddir}}'
    #         state: directory
    #         mode: '0755'
    #     - name: copy Dockerfile
    #       ansible.builtin.template:
    #         src: templates/Dockerfile.ubuntu22.j2
    #         dest: '{{dockerbuilddir}}/Dockerfile'
    #         mode: '0644'
    #     - name: copy Dockerfile entry point
    #       ansible.builtin.template:
    #         src: templates/docker-entrypoint.sh.j2
    #         dest: '{{dockerbuilddir}}/docker-entrypoint.sh'
    #         mode: '0644'

    #     # docker build . --no-cache --progress plain --network=host -t freeradius-ubuntu22
    #     - name: build container image
    #       become: true
    #       community.docker.docker_image:
    #         build:
    #           path: '{{dockerbuilddir}}'
    #           nocache: true
    #           network: host
    #         source: build
    #         name: '{{dockerregistry}}/{{dockername}}'
    #         tag: '{{dockertag}}'
    #         push: true
    #         state: present

    #     - name: archive container image as a tarball
    #       become: true
    #       community.docker.docker_image:
    #         name: '{{dockerregistry}}/{{dockername}}:{{dockertag}}'
    #         archive_path: '/tmp/{{dockername}}-{{dockertag}}.tar'
    #         source: pull
    #         state: present
    #       tags: dockerget
    #     # - name: fetch archived image
    #     #   fetch:
    #     #     src: /root/democontainer_v1_0.tar
    #     #     dest: ./democontainer_v1_0.tar
    #     #     flat: true
    #   tags: docker
